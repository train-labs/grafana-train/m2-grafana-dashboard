## Clase práctica: Creación de paneles y gráficos, opciones de visualización y personalización, y uso de alertas y notificaciones en Grafana

1. Configuración de fuentes de datos:
   - Configurar una fuente de datos en Grafana utilizando AWS CloudWatch.
   - Establecer la conexión y autenticación con la cuenta de AWS.

2. Creación de paneles de control para monitorear servicios de AWS:
   - Crear Directorio para agrupar dashboard.
   - Crear paneles de control específicos para servicios como:
        - Establecer Objetivos:
            - API Gateway
                - Objetivo 1:  Supervisar el tráfico de las APIs para identificar patrones de uso, picos de carga y posibles cuellos de botella.
                - Objetivo 2:Obtener métricas sobre la cantidad de solicitudes, errores, tiempos de respuesta y ancho de banda consumido por las APIs.
            - Lambda
                - Objetivo: Obtener métricas de ejecución, como el número de invocaciones, el tiempo de ejecución y el consumo de memoria, para analizar y mejorar el rendimiento de las funciones Lambda.
            - RDS
                - Objetivo: Supervisar el uso de recursos, como CPU, memoria y almacenamiento, para optimizar la capacidad y evitar sobrecargas.
            - EC2.
        - Añadir variables globales:

            EJ: Variables: Dashboard API Gateway:

                - datasource 
                - region > tipo: query > query type: region
                - api    > tipo: query > query type: dimension value > region: $region > namespace : AWS/Apigateway > Metric: Count > Dimensión key: Api Name > Dimension: null
   - Agregar gráficos y tablas para mostrar métricas relevantes de cada servicio.
        - Añadir grafico "Stat" con 4 Query:
            - Query 1 (Builder): Namespace: AWS/Apigateway > Metric name: Count > Statistic: Sum // Dimensión > ApiName = $api_name // id: total
            - Query 2 (Builder): Namespace: AWS/Apigateway > Metric name: 4XXError > Statistic: Sum // Dimensión > ApiName = $api_name // id: error4xx
            - Query 3 (Builder): Namespace: AWS/Apigateway > Metric name: 5XXError > Statistic: Sum // Dimensión > ApiName = $api_name // id: error5xx
            - Query 4 (code): SUM([error4xx, error5xx]) *100/total > ID: error > laberl Error Rate%

        - Añadir grafico "Time Series"
            - Query 1 (Builder): Namespace: AWS/Apigateway > Metric name: Latency > statistic: Average // Dimensión > ApiName = $api_name
            - Query 2 (Builder): Namespace: AWS/Apigateway > Metric name: integrationLatency > statistic: Average // Dimensión > ApiName = $api_name
            - Add threshhold > 
3. Uso de consultas y expresiones:
   - Utilizar consultas en Grafana para filtrar y seleccionar los datos deseados.
   - Aplicar expresiones y transformaciones a los datos para obtener información adicional.

4. Opciones de visualización y personalización:
   - Seleccionar el tipo de gráfico más adecuado según los datos a visualizar (líneas, barras, dispersión, etc.).
   - Configurar ejes, etiquetas y leyendas para mejorar la legibilidad y comprensión de los gráficos.

5. Configuración de alertas:
   - Establecer alertas en Grafana para monitorear métricas críticas.
   - Definir umbrales y condiciones para activar las alertas en caso de superar límites predefinidos.
