# AWS: API Gateway - Grafana monitoring 

Versión de Grafana: 7.1.5

Versión de panel: 1.0.3

Cloudwatch: 1.0.0

---

## Objetivo: 

Obtener métricas sobre la cantidad de solicitudes, errores, tiempos de respuesta y ancho de banda consumido por las APIs.

--- 
## Variables:

- datasource    : Origen de datos
- region        : Regiones expuestas por AWS
- api           : nombre de api
