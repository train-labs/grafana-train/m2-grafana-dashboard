# AWS: Lambda - Grafana monitoring 

Versión de Grafana: 7.1.5

Versión de panel: 1.0.0

Cloudwatch: 1.0.0

---

## Objetivo: 

Obtener métricas de ejecución, como el número de invocaciones, el tiempo de ejecución y el consumo de memoria, para analizar y mejorar el rendimiento de las funciones Lambda.

--- 
## Variables:

- datasource    : Origen de datos
- agg           : Intervalos de tiempo
- region        : Regiones expuestas por AWS
- functionname  : nombre de lambda
